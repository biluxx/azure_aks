#!/bin/bash

sunum=$(ls -ltr *sourcefile|wc -l)
echo "Searching for available source file"
if [ $sunum -gt 0 ];then
	echo "Found source file(s)"
	sourcefile=$(ls -1tr *sourcefile|tail -1)
	if [ $sunum -gt 1 ];then
		echo "Found multiple sourcefile , using the latest one:"
	fi
	echo -e "sourcefile:\t\t\t${sourcefile}"
	source ${sourcefile}
else
	echo "No sourcefile is present"
	exit 1
fi

function create_flapoyment() {
	unset DEFAULTPOD
	unset PODNAME
	case $1 in
		init)
			DEFAULTPOD="nominatiminit"
			;;
		database)
			DEFAULTPOD="nominatimdatabase"
			;;
		web)
			DEFAULTPOD="nominatimweb"
			;;
		tileserver)
			DEFAULTPOD="tileserver"
			;;
		*)
			DEFAULTPOD="nominatimtest"
			;;
	esac
	
	
	if [ "$1" == "web" ];then
		read -p "Please add the loadbalancer service and POD name for the deployment [${DEFAULTPOD}-lb for service and ${DEFAULTPOD} for the POD]:" PODNAME
		PODNAME=${PODNAME:-${DEFAULTPOD}}
		cp ./templates/${1}_deployment_template.yaml ${PODNAME}_deployment.yaml
		sed -i "s/__PODNAME/${PODNAME}/g" "${PODNAME}_deployment.yaml"
		read -p "Please add the loadbalancer public IP port number [80]:" LBPORT
		LBPORT=${LBPORT:-80}
		sed -i "s/__LBPORT/${LBPORT}/g" "${PODNAME}_deployment.yaml"
	else
		read -p "Please add the deployed POD/APP name[${DEFAULTPOD}]: " PODNAME
		PODNAME=${PODNAME:-${DEFAULTPOD}}
		cp ./templates/${1}_deployment_template.yaml ${PODNAME}_deployment.yaml
		sed -i "s/__PODNAME/${PODNAME}/g" "${PODNAME}_deployment.yaml"
	fi

	echo "List repositories:"
#	for i in `az acr repository list --name ${CR} -o tsv`; do echo "Repository: "$i;tags=$(for j in `az acr repository show-tags --name ${CR} --repository ${i} -o tsv`;do echo $j;done);echo "Tags:"; echo ${tags};done
	images=$(az acr repository list --name ${CR} -o tsv)
	DEFAULT_IMAGE=$(echo $images | awk '{print $NF}')
	echo $images
	read -p "Please add the image name of the remote container registry [${DEFAULT_IMAGE}]: " IMAGE_NAME
	IMAGE_NAME=${IMAGE_NAME:-${DEFAULT_IMAGE}}
	sed -i "s/__IMAGE/${IMAGE_NAME}/g" "${PODNAME}_deployment.yaml"
	echo "List tags of the image ${IMAGE_NAME}:"
	tags=$(az acr repository show-tags --name ${CR} --repository ${IMAGE_NAME} -o tsv)
	echo $tags
	DEFAULT_TAG=$(echo $tags | awk '{print $NF}')
	read -p "Please add the image ${IMAGE_NAME} label [${DEFAULT_TAG}]:" IMAGE_LABEL
	IMAGE_LABEL=${IMAGE_LABEL:-${DEFAULT_TAG}}
	sed -i "s/__LABEL/${IMAGE_LABEL}/g" "${PODNAME}_deployment.yaml"

	#getting disk uri
	#DISKID=$(az disk show --resource-group ${NODERG} --name ${DISKNAME} --query id | tr -d '"')

	sed -i "s/__CRNAME/${CR}/g" "${PODNAME}_deployment.yaml"
	sed -i "s/__CRSECRET_NAME/${CRSEC}/g" "${PODNAME}_deployment.yaml"
	sed -i "s/__SMBSECRET_NAME/${SMBSEC}/g" "${PODNAME}_deployment.yaml"
	sed -i "s/__DISKNAME/${DISKNAME}/g" "${PODNAME}_deployment.yaml"
	sed -i "s|__SHARE|${SHNAME}|g" "${PODNAME}_deployment.yaml"
	sed -i "s|__DISKID|${DISKURI}|g" "${PODNAME}_deployment.yaml"
        sed -i "s/__2NDSHARE/${SHNAME2}/g" "${PODNAME}_deployment.yaml"
        sed -i "s/__PVSECRET_NAME/${PVSEC}/g" "${PODNAME}_deployment.yaml"

	echo "deployment file creation was finised:"
	echo ""
	echo " - ${PODNAME}_deployment.yaml"
	echo "Please check it, after then you can deploy it with"
	echo "kubectl apply -f ${PODNAME}_deployment.yaml"
}

echo "Creating the Initialization deployment file..."
create_flapoyment init

echo "Creating the DATABASE deployment file..."
create_flapoyment database

echo "Creating the Webserver deployment file..."
create_flapoyment web 

echo "Creating the TILESERVER deployment file..."
create_flapoyment tileserver

