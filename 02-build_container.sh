#!/bin/bash

# functions 

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; exit 1 ;;
        esac
    done
}

echo "Building container to azure container registry"
echo "----------------------------------------------"

echo "Checking if you are logged in into the azure"
az account show || az login --use-device-code

echo "The tenant/subscription is correct?"
yes_or_no 


sunum=$(ls -ltr *sourcefile|wc -l)
echo "Searching for available source file"
if [ $sunum -gt 0 ];then 
	echo "Found source file(s)"
	sourcefile=$(ls -1tr *sourcefile|tail -1)
	if [ $sunum -gt 1 ];then 
		echo "Found multiple sourcefile , using the last one:"
	fi
	echo -e "sourcefile:\t\t\t${sourcefile}"
	source ${sourcefile}
else
	echo "No sourcefile is present"
	exit 1
fi

echo "Download sample .pbf file(malta-latest):"
wget http://download.geofabrik.de/europe/malta-latest.osm.pbf

echo "Upload sample .pbf file(malta-latest) to ${SHNAME}"
echo 'az storage file upload --account-name ${STA} --account-key ${STORAGEKEY} --share-name "osm" --source "malta-latest.osm.pbf" --path "malta-latest.osm.pbf"'
az storage file upload --account-name ${STA} --account-key ${STORAGEKEY} --share-name ${SHNAME} --source "malta-latest.osm.pbf" --path "malta-latest.osm.pbf"

##optional##############################################
#echo "Searching mbtiles in current directory"
#find . -iname "*.mbtiles" 
#read -p "Enter the mbtiles file path" MBTILESFILE
#echo "Uploading ${MBTILESFILE} to share ${SHNAME2}"
#read -p "Add the target mbtiles file name:" MBTILESNAME
#az storage file upload --account-name ${STA} --account-key ${STORAGEKEY} --share-name ${SHNAME2} --source ${MBTILESFILE} --path ${MBTILESNAME}
################################################

echo -e "Resource Group Name:\t\t$RG"
echo "###### IMAGE BUILD VARIABLES" >> ${sourcefile}
read -p "Enter Name of the new image [nominatim]: " IMAGE
IMAGE=${IMAGE:-nominatim}
echo "IMAGE=${IMAGE}" >> ${sourcefile}

read -p "Enter the label of the image ${IMAGE} [v1]: " IMG_LABEL
IMG_LABEL=${IMG_LABEL:-v1}
echo "IMAGE_LABEL=${IMG_LABEL}" >> ${sourcefile}

if [ -z ${CR} ];then 
	read -p "Enter the azure container registry name [test]:" CR
	CR=${CR:-test}
fi 	

echo "Searching for Dockerfiles in the current directory"
dockerfiles=$(find . -name "Dockerfile")
echo "Available Dockerfiles:"
echo ${dockerfiles} | sed -e "s/\/Dockerfile/ /g"
read -p "Enter the dockerfile path [./nominatim/3.1]" DFPATH
DFPATH=${DFPATH:-./nominatim/3.1}
echo "DFPATH=${DFPATH}" >> ${sourcefile}

echo "With the following parameters will the build start:"
echo "az acr build -t ${IMAGE}:${IMG_LABEL} -r ${CR} ${DFPATH}"
echo "Is this correct?[Yy/Nn]"
yes_or_no

echo "Building container..."
BUILD_START=$(TZ=UTC date "+%Y-%m-%d-%H:%M:%S")
az acr build -t ${IMAGE}:${IMG_LABEL} -r ${CR} ${DFPATH}
BUILD_STOP=$(TZ=UTC date "+%Y-%m-%d-%H:%M:%S")

echo "${BUILD_START} ---- BUILD ----> ${BUILD_STOP}" 
