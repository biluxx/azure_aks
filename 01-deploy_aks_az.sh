#!/bin/bash
# for colorizin the output 
c_rst='\033[0m'
c_red='\033[1;31m'
c_grn='\033[1;32m'
c_yel='\033[1;33m'
c_pur='\033[1;35m'
c_cyn='\033[1;36m'
c_wht='\033[1;37m'

# CONFIG
timestamp=$(TZ=UTC date "+%y%m%d%H%M")
logfile="${timestamp}-aks-deployment.log"
sourcefile="${timestamp}-sourcefile" 
###############
# functions 

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
	    [Nn]*) echo "Abort a sört forevör :), bye" ; exit 1 ;;
        esac
    done
}
###############
echo "Checking if you are logged in into the azure"
az account show || az login --use-device-code
echo "The tenant/subscription is correct[Yy/Nn]?"
yes_or_no
###############
echo -e "${c_wht}====================================================="
echo -e "========${c_rst} ${c_grn}Deployment Configuration Settings${c_rst} ${c_wht}========="
echo -e "=====================================================${c_rst}"
echo -e "Available locations:"
LOCATIONS=$(az account list-locations --query [].name -o tsv)
echo -e ${LOCATIONS}
read -p "1/. Enter location [westeurope]:" LOC
LOC=${LOC:-westeurope}
read -p "2/. Enter Resource Group name [RG-AKS]: " RG
RG=${RG:-RG-AKS}
read -p "3./ Enter new container registry name [cr${timestamp}]: " CR
CR=${CR:-cr${timestamp}}
read -p "4./ Enter new storage account name [sta${timestamp}]: " STA
STA=${STA:-sta${timestamp}}
echo -e "Storage account for AKS: ${STA}" >> ${logfile}
echo -e "STA=${STA}" >> ${sourcefile}
read -p "5./ Enter the share name for the osm file ${STA} [osm]: " SHNAME
SHNAME=${SHNAME:-osm}
read -p "5./ Enter the share name for the mbtiles file ${STA} [mbtiles]: " SHNAME2
SHNAME2=${SHNAME2:-mbtiles}
read -p "6./ Enter Name of the NEW SP [SP-AKS-001]: " SPAKS
SPAKS=${SPAKS:-SP-AKS-001}
echo -e "BE AWARE !!! that the creation process will also create in the user home .ssh directory"
echo -e "an ssh public and private key, so it is possible that the old one will be owerwritten!!!!"
echo -e "If it so then please open another terminal and create a backup of the actual one!!!!"
echo -e "Available kubernetes versions on the location ${LOC}:"
AKS_VERSIONS=$(az aks get-versions --location ${LOC} --query orchestrators[].orchestratorVersion -o tsv)
echo -e ${AKS_VERSIONS} | sed -e "s/ /  /g" 
LATEST_AKS=$(echo -e ${AKS_VERSIONS}|awk '{print $NF}')
read -p "7./ Choose the aks version [${LATEST_AKS}]:" AKSVER
AKSVER=${AKSVER:-${LATEST_AKS}}
read -p "8./ Enter Name of the AKS [AKS-001]: " AKS
AKS=${AKS:-AKS-001}
read -p "9./ Enter Node count of the AKS ${AKS} [1]: " AKSNC
AKSNC=${AKSNC:-1}
read -p "10./ Enter Name of the docker-registry secret [crsecret001]: " CRSEC
CRSEC=${CRSEC:-crsecret001}
read -p "11./ Enter email of the docker-registry secret [secret001@example.com]: " CREMAIL
CREMAIL=${CREMAIL:-secret001@example.com}
read -p "12./ Enter Name of the file share secret [smbsecret001]: " SMBSEC
SMBSEC=${SMBSEC:-smbsecret001}
read -p "13./ Enter Name of the persistent volume secret [pvsecret001]: " PVSEC
PVSEC=${PVSEC:-pvsecret001}
read -p "14./ Enter Name of the managed (ssd) disk for the postgresql ${AKS} [PSQLDISK-001]: " DISKNAME
DISKNAME=${DISKNAME:-PSQLDISK-001}
read -p "15./ Enter the size of the disk ${DISKNAME} in gb [5]: " DISKSIZE
DISKSIZE=${DISKSIZE:-5}
#read -p "16./ Enter Name of the managed (ssd) disk for the varnish cache ${AKS} [VARNDISK-001]: " DISKNAME2
#DISKNAME2=${DISKNAME2:-VARNDISK-001}
#read -p "17./ Enter the size of the disk ${DISKNAME2} in gb [1]: " DISKSIZE2
#DISKSIZE2=${DISKSIZE2:-1}

echo -e "${c_wht}====================================================="
echo -e "================${c_rst} ${c_grn}Starting FLAPPING${c_rst} ${c_wht}=================="
echo -e "=====================================================${c_rst}"
FLAP_START=$(TZ=UTC date "+%Y-%m-%d-%H:%M:%S")
#echo -e "====================================================="
echo -e "${c_cyn}Create Resource Group${c_rst}"
az group create --name ${RG} --location ${LOC}
echo -e "AKS Resource Group: ${RG}" >> ${logfile}
echo -e "RG=${RG}" >> ${sourcefile}
echo -e "Location: ${LOC}" >> ${logfile}
echo -e "LOC=${LOC}" >> ${sourcefile}
#
##############################
echo -e "${c_cyn}Create Container Registry ${c_grn}${CR}${c_rst}"
az acr create --resource-group ${RG} --name ${CR} --sku Basic
echo -e "Container Registry: ${CR}" >> ${logfile}
echo -e "CR=${CR}" >> ${sourcefile}
#
##############################
echo -e "${c_cyn}Create Storage Account${c_rst}"
az storage account create --name ${STA} --resource-group ${RG} --location ${LOC} --kind StorageV2 --access-tier Hot --sku Standard_LRS
#
##############################
echo -e "${c_cyn}Get storage key of the storage account ${c_grn}${STA}${c_grn}...${c_rst}"
STORAGEKEY=$(az storage account keys list --resource-group ${RG} --account-name ${STA} --query "[0].value" | tr -d '"')
echo -e "Storage key is: ${STORAGEKEY}"
echo -e "Creating file share ${SHNAME} and ${SHNAME2} on ${STA}"
az storage share create --account-name ${STA} --account-key ${STORAGEKEY} --name "${SHNAME}"
az storage share create --account-name ${STA} --account-key ${STORAGEKEY} --name "${SHNAME2}"
echo -e "Storage key: ${STORAGEKEY}" >> ${logfile}
echo -e "Share name1: ${SHNAME}" >> ${logfile}
echo -e "Share name2: ${SHNAME2}" >> ${logfile}
echo -e "STORAGEKEY=${STORAGEKEY}" >> ${sourcefile}
echo -e "SHNAME=${SHNAME}" >> ${sourcefile}
echo -e "SHNAME2=${SHNAME2}" >> ${sourcefile}
#
##############################
SERVICE_PRINCIPAL_NAME=${SPAKS};
ACR_LOGIN_SERVER=$(az acr show --name ${CR} --query loginServer --output tsv)
ACR_REGISTRY_ID=$(az acr show --name ${CR} --query id --output tsv)
echo -e "${c_cyn}Creating SP and adding role assignment of the default AKS SP to the container registry ${c_grn}${CR}${c_rst}"
SP_PASSWD=$(az ad sp create-for-rbac --name http://$SERVICE_PRINCIPAL_NAME --role acrpull --scopes $ACR_REGISTRY_ID --query password --output tsv)
CLIENT_ID=$(az ad sp show --id http://$SERVICE_PRINCIPAL_NAME --query appId --output tsv)
#echo -e ". Login into container registry ${CR}"
#az acr login -n ${CR}
SUB_ID=$(az account show --query id -o tsv)
echo -e "Service principal ID: $CLIENT_ID";echo -e "Service principal password: $SP_PASSWD"
echo -e "SP Name: ${SPAKS}" >> ${logfile}
echo -e "SP App Id: ${CLIENT_ID}" >> ${logfile}
echo -e "SP Password: ${SP_PASSWD}" >> ${logfile}
echo -e "Subscription Id: ${SUB_ID}" >> ${logfile}
echo -e "SERVICE_PRINCIPAL_NAME=${SERVICE_PRINCIPAL_NAME}" >> ${sourcefile}
echo -e "SP_PASSWD=${SP_PASSWD}" >> ${sourcefile}
echo -e "SP_APPID=${CLIENT_ID}" >> ${sourcefile}
echo -e "SUB_ID=${SUB_ID}" >> ${sourcefile}

echo -e "Adding role assignment to the subscription"
az role assignment create --assignee ${CLIENT_ID} --role Contributor --scope /subscriptions/${SUB_ID}
#
##############################
echo "Clean up previously created SP in ~/.azure, when exists."
# Becaouse of this , aaaahhhwww:
#  Warning  FailedAttachVolume  24s   attachdetach-controller  AttachVolume.Attach failed for volume "data" : failed to get azure instance id for node "aks-nodepool1" (azure.BearerAuthorizer#WithAuthorization: Failed to refresh the Token for request to https://management.azure.com/subscriptions/#################/resourceGroups/###################/providers/Microsoft.Compute/virtualMachines/aks-nodepool1?%!!(MISSING)e(MISSING)xpand=instanceView&api-version=2018-04-01: StatusCode=401 -- Original Error: adal: Refresh request failed. Status Code = '401'. Response body: {"error":"invalid_client","error_description":"AADSTS7000215: Invalid client secret is provided.\r\nTrace ID: X\r\nCorrelation ID: Y\r\nTimestamp: 2019-03-09 15:53:21Z","error_codes":[7000215],"timestamp":"2019-03-09 15:53:21Z","trace_id":"X","correlation_id":"Y"})

if [ -f ~/.azure/aksServicePrincipal.json ];then 
	echo -e "${c_cyn}Previous aks sp exists, create a backup.${c_rst}"
	mv ~/.azure/aksServicePrincipal.json ~/.azure/${timestamp}-aksServicePrincipal.backupjson
else
	echo -e "${c_green}File does'nt exists, good to go...${c_rst}"
fi


#
##############################
echo -e "${c_cyn}Creating aks ${c_grn}${AKS}${c_cyn}...${c_rst}"

#When deployment start and try to attach the disk , we get following error, probably some password encryption or role assignment is missing (?)
#az aks create --resource-group ${RG} --name ${AKS} --kubernetes-version $AKSVER --node-count ${AKSNC} --service-principal ${CLIENT_ID} --client-secret ${SP_PASSWD} --generate-ssh-keys && echo -e "done"
az aks create --resource-group ${RG} --name ${AKS} --kubernetes-version ${AKSVER} --node-count ${AKSNC} --generate-ssh-keys && echo -e "done"

echo -e "====================================================="
echo -e "Azure Aks Name: ${AKS}" >> ${logfile}
echo -e "Azure Aks Version: ${AKSVER}" >> ${logfile}
echo -e "Azure Aks nodes number: ${AKSNC}" >> ${logfile}
echo -e "AKS=${AKS}" >> ${sourcefile}
echo -e "AKSVER=${AKSVER}" >> ${sourcefile}
echo -e "AKSNC=${AKSNC}" >> ${sourcefile}

echo -e "${c_ylw}Installing kubectl and other fine stuff...(with sudo) when it's necessary.${c_rst}"
if [ -f /usr/local/bin/kubectl ]; then
   echo -e "${c_grn}kubectl is installed, no need to install.${c_rst}"
else
   echo -e "${c_cyn}Installing kubectl and other stuffz..${c_rst}"
   sudo az aks install-cli
fi
echo -e "====================================================="


echo -e "${c_ylw}Create previous backup of the file ${HOME}/.docker/config.json, when exists${c_rst}"
if [ -f ${HOME}/.docker/config.json ]; then
   echo -e "${c_cyn}Creating a backup.${c_rst}"
   mv ${HOME}/.docker/config.json ${HOME}/.docker/config.backup${timestamp}
else
   echo -e "${c_grn}Config file not exists.${c_rst}"
fi
echo -e "====================================================="

echo -e "${c_ylw}Clean up aks cluster configuration${c_rst}"
if [ -f ${HOME}/.kube/config ]; then
   echo -e "${c_cyn}Creating a backup.${c_rst}"
   mv ${HOME}/.kube/config ${HOME}/.kube/backup
   #kubectl config delete-cluster --all
else
   echo -e "${c_grn}Config file not exists.${c_rst}"
fi
echo -e "====================================================="

echo -e "${c_cyn}Login into aks cluster${c_rst}"
az aks get-credentials --resource-group ${RG} --name ${AKS}
echo -e "====================================================="

#echo -e "${c_cyn}Login into container registry${c_rst}"
#az acr login --name ${CR}
#echo -e "====================================================="
#
##############################
echo -e "${c_cyn}Creating container-registry secret ${CRSEC}${c_rst}"
kubectl create secret docker-registry ${CRSEC} --docker-server=${CR}.azurecr.io --docker-username=${CLIENT_ID} --docker-password=${SP_PASSWD} --docker-email ${CREMAIL}
echo -e "====================================================="
echo -e "Container Registry Secret Name: ${CRSEC}" >> ${logfile}
echo -e "Container Registry Email: ${CREMAIL}" >> ${logfile}
echo -e "CRSEC=${CRSEC}" >> ${sourcefile}
echo -e "CREMAIL=${CREMAIL}" >> ${sourcefile}
#
##############################
echo -e "${c_cyn}Creating secret for the azureFile share(s) ${c_grn}${SMBSEC}${c_rst}${c_cyn}...${c_rst}"
kubectl create secret generic ${SMBSEC} --from-literal=azurestorageaccountname="${STA}" --from-literal=azurestorageaccountkey="${STORAGEKEY}" --type="microsoft.com/smb"
echo -e "====================================================="
echo -e "File share secret name: ${SMBSEC}" >> ${logfile}
echo -e "SMBSEC=${SMBSEC}" >> ${sourcefile}
#
##############################
echo -e "${c_cyn}Creating secret for persistent volume(s) ${c_grn}${SMBSEC}${c_rst}${c_cyn}...${c_rst}"
kubectl create secret generic ${PVSEC} --from-literal=azurestorageaccountname="${STA}" --from-literal=azurestorageaccountkey="${STORAGEKEY}" --type="Opaque"
echo -e "====================================================="
echo -e "Persistent volume secret name: ${PVSEC}" >> ${logfile}
echo -e "PVSEC=${PVSEC}" >> ${sourcefile}
#
##############################
echo -e "${c_cyn}Creating managed(ssd) disk for the aks cluster ${c_grn}${AKS}${c_rst}${c_cyn}:${c_rst}"
NODERG=$(az aks show --resource-group ${RG} --name ${AKS} --query nodeResourceGroup -o tsv)
DISKURI=$(az disk create --resource-group ${NODERG} --name ${DISKNAME} --size-gb ${DISKSIZE} --query id --output tsv)
echo -e "Disk ResG.: ${NODERG}" >> ${logfile}
echo -e "NODERG=${NODERG}" >> ${sourcefile}
echo -e "Postgresql Disk: ${DISKNAME}" >> ${logfile}
echo -e "Postgresql Disk Size: ${DISKSIZE}" >> ${logfile}
echo -e "Postgresql Disk Uri: ${DISKURI}" >> ${logfile}
echo -e "DISKNAME=${DISKNAME}" >> ${sourcefile}
echo -e "DISKSIZE=${DISKSIZE}" >> ${sourcefile}
echo -e "DISKURI=${DISKURI}" >> ${sourcefile}
#DISKURI2=$(az disk create --resource-group ${NODERG} --name ${DISKNAME2} --size-gb ${DISKSIZE2} --query id --output tsv)
#echo -e "Varnish Cache Disk: ${DISKNAME2}" >> ${logfile}
#echo -e "Varnish Disk Size: ${DISKSIZE2}" >> ${logfile}
#echo -e "Varnish Disk Uri: ${DISKURI2}" >> ${logfile}
#echo -e "DISKNAME2=${DISKNAME2}" >> ${sourcefile}
#echo -e "DISKSIZE2=${DISKSIZE2}" >> ${sourcefile}
#echo -e "DISKURI2=${DISKURI2}" >> ${sourcefile}

echo -e "${c_wht}====================================================="
echo -e "============ ${c_grn}FLAPOYMENT FINISHED ${c_wht}===================="
echo -e "=====================================================${c_rst}"
FLAP_STOP=$(TZ=UTC date "+%Y-%m-%d-%H:%M:%S")
echo -e "${FLAP_START} ----- FLAPPING ------> ${FLAP_STOP}"
echo -e "${FLAP_START} ----- FLAPPING ------> ${FLAP_STOP}" >> ${logfile}
echo -e "After the aks is not needed you can delete the resourcegroups with:"
echo -e "${c_cyn}az group delete --name ${c_grn}${NODERG}${c_cyn} --yes --no-wait;az group delete --name ${c_grn}${RG}${c_cyn} --yes --no-wait;az group delete --name NetworkWatcherRG --yes --no-wait;az ad sp delete --id ${c_grn}${CLIENT_ID}${c_rst}"
echo -e "After the aks is not needed you can delete the resourcegroups with:" >> ${logfile}
echo -e "az group delete --name ${NODERG} --yes --no-wait;az group delete --name ${RG} --yes --no-wait;az group delete --name NetworkWatcherRG --yes --no-wait;az ad sp delete --id ${CLIENT_ID}" >> ${logfile}
