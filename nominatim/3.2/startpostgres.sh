#!/bin/bash

function configure() {
        echo "Get cpu core number of the machine"
        numofcores=$(cat /proc/cpuinfo |grep processor |wc -l)

        echo "Get memory information of the machine"
        memory="$(free -h |grep Mem | awk '{print $2}')B"

        echo "Get architecture"
        architecture=$(arch)
        drive="SSD"
        max_connections="100"
        environment="DW"
        pg_ver="10"

        echo "Creating backup"
        cp -p /etc/postgresql/10/main/postgresql.conf /etc/postgresql/10/main/postgresql.conf.bak
        echo "Getting a configuration"
        curl 'https://api.pgconfig.org/v1/tuning/get-config?arch='${architecture}'&drive_type='${drive}'&environment_name='${environment}'&format=conf&max_connections='${max_connections}'&os_type=Linux&pg_version='${pg_ver}'&total_ram='${memory} |grep -v '^#' | sed -r '/^\s*$/d' > temporary.conf
        echo "Cleanup old configuration"
        while read line; do string=$(echo $line|awk '{print $1}');sed -i "/^$string/d" /etc/postgresql/10/main/postgresql.conf;done <temporary.conf

        echo "Fill up configuration with good things"
        while read line; do echo $line >> /etc/postgresql/10/main/postgresql.conf;done <temporary.conf

        echo "Show differences"
        diff /etc/postgresql/10/main/postgresql.conf /etc/postgresql/10/main/postgresql.conf.bak

}

configure
sed -i "s/\/var\/lib\/postgresql\/10\/main/\/data\/postgresql\/10\/main/g" /etc/postgresql/10/main/postgresql.conf
chown -R postgres:postgres /data/postgresql
service postgresql start
tail -f /var/log/postgresql/postgresql-10-main.log
