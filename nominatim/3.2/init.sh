OSMFILE=$1

configure() {
        echo "Get cpu core number of the machine"
        THREADS=$(cat /proc/cpuinfo |grep processor |wc -l)

        echo "Get memory information of the machine"
        memory="$(free -h |grep Mem | awk '{print $2}')B"

        echo "Get architecture"
        architecture=$(arch)
        drive="SSD"
        max_connections="100"
        environment="DW"
        pg_ver="10"

        echo "Creating backup"
        cp -p /etc/postgresql/10/main/postgresql.conf /etc/postgresql/10/main/postgresql.conf.bak
        echo "Getting a configuration"
        curl 'https://api.pgconfig.org/v1/tuning/get-config?arch='${architecture}'&drive_type='${drive}'&environment_name='${environment}'&format=conf&max_connections='${max_connections}'&os_type=Linux&pg_version='${pg_ver}'&total_ram='${memory} |grep -v '^#' | sed -r '/^\s*$/d' > temporary.conf
	echo "fsync = off" >> temporary.conf
	echo "full_page_writes = off" >> temporary.conf

        echo "Cleanup old configuration"
        while read line; do string=$(echo $line|awk '{print $1}');sed -i "/^$string/d" /etc/postgresql/10/main/postgresql.conf;done <temporary.conf

        echo "Fill up configuration with good things"
        while read line; do echo $line >> /etc/postgresql/10/main/postgresql.conf;done <temporary.conf

        echo "Show differences"
        diff /etc/postgresql/10/main/postgresql.conf /etc/postgresql/10/main/postgresql.conf.bak
}


if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters, parameter numbers: ${#}"
    echo "usage: $0 <OSM file path>"
    exit 1
fi

if [ -e /data/database_initialization_done ]; then
	echo "You cannot start initialization on an already used disk"
	echo "Script will be exit, bye"
	exit 1
else 
	echo "STARTING INITIALIZATION"
	echo "======================="
fi 

echo "1 Stopping postgresql"
service postgresql stop
service postgresql status
echo "1.5 Sleeping 10 sec"
sleep 10
configure 
echo "2 Create dir /data/postgresql/10/main"
mkdir -p /data/postgresql/10/main
chown -R postgres:postgres /data/postgresql
echo "Initialize new db directory"
sudo -H -E -u postgres bash -c '/usr/lib/postgresql/10/bin/initdb -D /data/postgresql/10/main'
echo "4 Remove old postgresql data"
rm -rf /var/lib/postgresql/10/main
chown -R postgres:postgres /data/postgresql
echo "5 Editing config file, change data dir to remote one"
sed -i "s/\/var\/lib\/postgresql\/10\/main/\/data\/postgresql\/10\/main/g" /etc/postgresql/10/main/postgresql.conf
echo "6 Starting postgresql"
service postgresql start
service postgresql status
echo "7 Creating sql user nominatim"
sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='nominatim'" | grep -q 1 || sudo -u postgres createuser -s nominatim
echo "8 Creating sql user www-data"
sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='www-data'" | grep -q 1 || sudo -u postgres createuser -SDR www-data
echo "9 Delete,drop previously created nominatim db if exists"
sudo -u postgres psql postgres -c "DROP DATABASE IF EXISTS nominatim"
echo "10 Create os user nominatim"
useradd -m -p password1234 nominatim
echo "11 Chowning /apps/src to nominatim"
chown -R nominatim:nominatim ./src
sudo -u nominatim ./src/build/utils/setup.php --osm-file $OSMFILE --all --threads $THREADS
sudo -H -E -u postgres bash -c 'psql -d nominatim -c "CREATE INDEX nodes_index ON public.planet_osm_ways USING gin (nodes);"'
echo "12 Stopping postgresql on the last time"
service postgresql stop
service postgresql status
echo "INITIALIZATION FINISHED"
echo "======================="
touch /data/database_initialization_done
echo "Going to in infinity loop..you can delete the deployment"
/bin/bash -c "trap : TERM INT; sleep infinity & wait"
