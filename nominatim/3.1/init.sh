OSMFILE=$1
THREADS=$2

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters, parameter numbers: ${#}"
    echo "usage: $0 <OSM file path> <Number of threads>"
    exit 1
fi

if [ -e /data/database_initialization_done ]; then
	echo "You cannot start initialization on an already used disk"
	echo "Script will be exit, bye"
	exit 1
else 
	echo "STARTING INITIALIZATION"
	echo "======================="
fi 

echo "1 Stopping postgresql"
service postgresql stop
service postgresql status
echo "1.5 Sleeping 10 sec"
sleep 10
echo "2 Create dir /data/postgresql/9.5/main"
mkdir -p /data/postgresql/9.5/main
chown -R postgres:postgres /data/postgresql
echo "3 Syncing /var/lib/postgresql/9.5/main/ to /data/postgresql/9.5/main/"
rsync -avzr /var/lib/postgresql/9.5/main/ /data/postgresql/9.5/main/
echo "4 Remove old postgresql data"
rm -rf /var/lib/postgresql/9.5/main
chown -R postgres:postgres /data/postgresql
echo "5 Editing config file, change data dir to remote one"
sed -i "s/\/var\/lib\/postgresql\/9.5\/main/\/data\/postgresql\/9.5\/main/g" /etc/postgresql/9.5/main/postgresql.conf
echo "6 Starting postgresql"
service postgresql start
service postgresql status
echo "7 Creating sql user nominatim"
sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='nominatim'" | grep -q 1 || sudo -u postgres createuser -s nominatim
echo "8 Creating sql user www-data"
sudo -u postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='www-data'" | grep -q 1 || sudo -u postgres createuser -SDR www-data
echo "9 Delete,drop previously created nominatim db if exists"
sudo -u postgres psql postgres -c "DROP DATABASE IF EXISTS nominatim"
echo "10 Create os user nominatim"
useradd -m -p password1234 nominatim
echo "11 Chowning /apps/src to nominatim"
chown -R nominatim:nominatim ./src
sudo -u nominatim ./src/build/utils/setup.php --osm-file $OSMFILE --all --threads $THREADS
sudo -H -E -u postgres bash -c 'psql -d nominatim -c "CREATE INDEX nodes_index ON public.planet_osm_ways USING gin (nodes);"'
echo "12 Stopping postgresql on the last time"
service postgresql stop
service postgresql status
echo "INITIALIZATION FINISHED"
echo "======================="
touch /data/database_initialization_done
echo "Going to in infinity loop..you can delete the deployment"
/bin/bash -c "trap : TERM INT; sleep infinity & wait"
