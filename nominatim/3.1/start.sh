#!/bin/bash

stopServices() {
        service apache2 stop
        service postgresql stop
}
trap stopServices TERM

sed -i "s/\/var\/lib\/postgresql\/9.5\/main/\/data\/postgresql\/9.5\/main/g" /etc/postgresql/9.5/main/postgresql.conf
service postgresql start
service apache2 start

# fork a process and wait for it
tail -f /var/log/postgresql/postgresql-9.5-main.log &
wait
